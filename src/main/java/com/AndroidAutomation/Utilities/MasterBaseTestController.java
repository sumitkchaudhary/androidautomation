/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 04-Dec-2021
 *  FILE NAME  		: 	 MasterBaseTestController.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    1:32:55 am
 */
package com.AndroidAutomation.Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;

public class MasterBaseTestController {
	public static Properties pr= getPropertiesFileKey("EnvConfigure");
	//Method for device and deviceBrowser configuration
	public static AndroidDriver<MobileElement>  appDriver;
	
	private static DesiredCapabilities capabilities;
	public static AndroidDriver<MobileElement> deviceCapabilities(String deviceType, String apkFileName) {
		
		try {
			capabilities=new DesiredCapabilities();
			/*//Open this line if you want to run the case via maven file on desire device.
			 * run the command for run 
			 * mvn test D<variable name>=emulatorName -variable name which you define under System.getProperty("variableName")
			 * mvn test DdeviceName=SumitPixelEmulator    
			 * */
			//deviceType=System.getProperty("deviceName");   
			
			if(deviceType.contains("Emulator") || deviceType.contains("emulator")) {
				startAndroidEmulatorFromBatFile();
				Thread.sleep(7000);
				deviceType=pr.getProperty("pixelEmulator"); //this line fetch the emulator name from environment config file. NOTE: Must be comment this code when you going to run from maven.
				capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceType); //Here I given emulator name for now
			}else if(deviceType.contains("Real Device")) {
				capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Device");//here I given read device. Read device must be connect with system 
				
			}
			//capabilities.setCapability(MobileCapabilityType.APP, GetFilePathByNameHandler.getAbsolutPath("ApiDemos-debug.apk"));
			capabilities.setCapability(MobileCapabilityType.APP, getAbsolutPath(pr.getProperty(apkFileName)));
			// UI Automator  -- created by android to automate the android app  which calles as uiautomator2
			capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
			
			appDriver=new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
			
		} catch (Exception e) {
			e.getMessage();
			e.getStackTrace();
		}
		return appDriver;
		
	}
	
	public static  AndroidDriver<MobileElement> deviceBrowserCapabilities(String deviceType) 
	{
		try {
			capabilities = new DesiredCapabilities();
			if(deviceType.equals("Emulator")) {
				capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "SumitPixelEmulator");   
			}else if(deviceType.equals("Real Device")) {
				capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Device");//here I given read device. Read device must be connect with system 
			}
			capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
		    capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, BrowserType.CHROME);

		   // capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, MobileBrowserType.CHROME);
		    appDriver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

		} catch (Exception e) {
			e.getStackTrace();
			e.getMessage();
		}
		return appDriver;
	}

	public static AppiumDriverLocalService service;
	//Method help to start the appium automatically by script.
		public static AppiumDriverLocalService startAppiumServer() {
			boolean flag=	checkIfServerIsRunnning(4723);
			service=AppiumDriverLocalService.buildDefaultService();
			if(!flag) {
				System.out.println("Appium Server is starting, please wait....");
				service.start();
				System.out.println("Appium is start");
			}
			return service;
		}

		public static void killAppiumServerForceFullyByCommand(){
			try {
				Runtime.getRuntime().exec(getAbsolutPath("KillAppiumserverForceFully.bat"));
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}

		//This method will check is the appium server is in use or not 
		public static boolean checkIfServerIsRunnning(int port) {
			boolean isServerRunning = false;
			ServerSocket serverSocket;
			try {
				serverSocket = new ServerSocket(port);
				
				serverSocket.close();
			} catch (IOException e) {
				//If control comes here, then it means that the port is in use
				isServerRunning = true;
			} finally {
				serverSocket = null;
			}
			return isServerRunning;
		}
		
		//This method will help to run the emulator from bat file
		public static void startAndroidEmulatorFromBatFile(){
			try {
				Runtime.getRuntime().exec(getAbsolutPath("androidEmulatorRunnerCommand.bat"));
				Thread.sleep(7000);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	public static void main(String[] args) {
		startAndroidEmulatorFromBatFile();
		startAppiumServer();
	}
		
		//Method is use for run the cmd commnd to execute Android Emmulator
		/*
		public static void executeAnroidEmulator(String emulatorName){
			
			try {
				Process process = Runtime.getRuntime().exec(
				        new String[]{"cmd", "/c", "emulator -avd "+emulatorName},
				        null, 
				        new File(pr.getProperty("avdEmulatorDirPath")));
				
				
				BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			    String line = "";
			    while ((line = reader.readLine()) != null) {
			        System.out.println(line);
			    }
			    Thread.sleep(5000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}*/
		
		//Method will take screenShot in when error will catch
		public static void getScreenShot(String fileName) throws IOException {
			File screenShotFilePath=((TakesScreenshot)appDriver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenShotFilePath, new File(System.getProperty("user.dir")+File.separator+"ScreenShotsContainer"+File.separator+fileName+".jpg"));
		}

		public static Properties getPropertiesFileKey(String fileName) {
			Properties getProperty=new Properties();
			try {
			FileInputStream getPropertiesFilePath=new FileInputStream(getAbsolutPath(fileName+".properties"));

			getProperty.load(getPropertiesFilePath);
		} catch (Exception e) {
			e.getMessage();
			e.getStackTrace();
		}
		return getProperty;
	}

	public static String getAbsolutPath(String fileName) {
		//File root setup
		File root = new File(System.getProperty("user.dir"));
		String fileAbolutePath ="";
		try {
			boolean recursive = true;
			//Collection method
			Collection<File> files = FileUtils.listFiles(root, null, recursive);
			for (Iterator<File> iterator = files.iterator(); iterator.hasNext();) {
				File file = (File) iterator.next();
				if (file.getName().equals(fileName))
					fileAbolutePath=file.getAbsolutePath();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileAbolutePath;
	}

	public static String fetchCurrentDateTime() {
		return new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
	}

}
