/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 14-Dec-2021
 *  FILE NAME  		: 	 HandleScrolling.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    9:32:42 am
 */
package com.AndroidAutomation.Utilities;

import org.openqa.selenium.interactions.touch.TouchActions;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class HandleScrolling {
	static AndroidDriver<MobileElement> appDriver;
	
	public HandleScrolling(AndroidDriver<MobileElement> appDriver) {
		this.appDriver=appDriver;
	}
	
	public static void scrollList(String desireItem) {
		appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\""+desireItem+"\"));");
	}
	
	public static void listScroll(String desireItem) {
		appDriver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\""+desireItem+"\").instance(0))"));
	}
	
	public static void scrollTheList(AndroidElement element, int x, int y) {
		TouchActions action=new TouchActions(appDriver);
		action.scroll(element, x, y);
	}
	

}
