/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 17-Feb-2022
 *  FILE NAME  		: 	 CheckIfAppIsInstalled.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    9:12:29 pm
 */
package com.Basics.RnD;

import com.AndroidAutomation.Utilities.MasterBaseTestController;
import org.testng.annotations.Test;


public class CheckIfAppIsInstalled extends MasterBaseTestController {
	
	@Test
	public void verifyAppIsInstalledAlready() {
		//
		service=startAppiumServer();
		//appDriver=deviceCapabilities("Emulator", "practiDemoApp");
		String appPackageStr="io.appium.android.apis";
		appDriver=deviceCapabilities("Emulator","ApiDemosdebug.apk");
		if(appDriver.isAppInstalled(appPackageStr)) {
			
			System.out.println("App is installed");
		}else {
			System.out.println("App is not installed");
			//appDriver=deviceCapabilities("Emulator", "practiDemoApp");
		}
		
		killAppiumServerForceFullyByCommand();
		
		
	}

}
