/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 30-Dec-2021
 *  FILE NAME  		: 	 FetchAPIDemosAPKFileBaseTestController.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    10:08:13 pm
 */
package com.Basics.APIDemoApp.BaseClass;

import com.AndroidAutomation.Utilities.MasterBaseTestController;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FetchAPIDemosAPKFileBaseTestController extends MasterBaseTestController {

	@BeforeMethod
	public void getAPKFile() {
		killAppiumServerForceFullyByCommand();
		service = startAppiumServer();
		appDriver=deviceCapabilities("Emulator", "practiceDemoApp");
		
		//appDriver=deviceCapabilities("Real Device", "practiDemoApp");
		
	}
	@AfterMethod
	public void stopAppium() {
		service.stop();
		System.out.println("Appium is successfully stop.");
	}

}
