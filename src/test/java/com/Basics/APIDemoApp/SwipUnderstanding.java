/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 12-Dec-2021
 *  FILE NAME  		: 	 SwipUnderstanding.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    9:41:58 pm
 */
package com.Basics.APIDemoApp;

import com.Basics.APIDemoApp.BaseClass.FetchAPIDemosAPKFileBaseTestController;
import org.testng.annotations.Test;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.offset.ElementOption;

public class SwipUnderstanding extends FetchAPIDemosAPKFileBaseTestController {
	@Test
	public void verifySetDateFunctionality() {
		/*
		 *	Test Cases 
		 *	- Open an app
		 *	- Click views
		 *	- Tap Date Widgets
		 *	- Tap 2. Inline
		 *	- Click on 9 
		 *	- Long press from 15 and release 35   
		 * */
		
		appDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		appDriver.findElementByXPath("//android.widget.TextView[@text='Views']").click();
		appDriver.findElementByXPath("//android.widget.TextView[@text='Date Widgets']").click();
		appDriver.findElementByXPath("//android.widget.TextView[@text='2. Inline']").click();
		appDriver.findElementByXPath("//*[@content-desc='9']").click();
		
		WebElement startPoint=appDriver.findElementByXPath("//*[@content-desc='15']");
		WebElement endPoint=appDriver.findElementByXPath("//*[@content-desc='35']");
		
		TouchAction touchPad=new TouchAction(appDriver);
		
		touchPad.longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(startPoint)).withDuration(Duration.ofSeconds(2))).moveTo(ElementOption.element(endPoint)).release().perform();
		
	}

}
