/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 11-Dec-2021
 *  FILE NAME  		: 	 GestureUnderstanding.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    11:29:07 pm
 */
package com.Basics.APIDemoApp;

import org.testng.annotations.Test;

import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Basics.APIDemoApp.BaseClass.FetchAPIDemosAPKFileBaseTestController;

import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;

public class GestureUnderstanding extends FetchAPIDemosAPKFileBaseTestController {
	
	@Test
	public void verifyLongPressTab(){
		/*
		 *	Test Cases 
		 *	- Open an app
		 *	- Click views
		 *	- Tap Expandable Lists
		 *	- Tap Custom Adapter
		 *	- Long press on people names 
		 *	- Verify the text on receive pop-up and verify the visible text  
		 * */
		
		appDriver.findElement(By.xpath("//android.widget.TextView[@text='Views']")).click();
		
		@SuppressWarnings("rawtypes")
		TouchAction tap=new TouchAction(appDriver);
		WebElement expandableList=appDriver.findElement(By.xpath("//android.widget.TextView[@text='Expandable Lists']"));
		tap.tap(TapOptions.tapOptions().withElement(ElementOption.element(expandableList))).perform();
		
		WebElement simpleAdaptar=appDriver.findElement(By.xpath("//android.widget.TextView[@text='1. Custom Adapter']"));
		tap.tap(TapOptions.tapOptions().withElement(ElementOption.element(simpleAdaptar))).perform();
		WebElement peopleName=appDriver.findElement(By.xpath("//android.widget.TextView[@text='People Names']"));
		tap.longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(peopleName)).withDuration(Duration.ofSeconds(2))).release().perform();
		WebElement popupTxt=appDriver.findElement(By.xpath("//android.widget.TextView[@text='Sample menu']"));
		System.out.println(popupTxt.getText());
		System.out.println(popupTxt.isDisplayed());
	}
}
