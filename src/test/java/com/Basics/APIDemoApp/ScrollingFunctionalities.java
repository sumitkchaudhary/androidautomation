/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 12-Dec-2021
 *  FILE NAME  		: 	 ScrollingFunctionalities.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    10:23:29 pm
 */
package com.Basics.APIDemoApp;

import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

import com.Basics.APIDemoApp.BaseClass.FetchAPIDemosAPKFileBaseTestController;

public class ScrollingFunctionalities extends FetchAPIDemosAPKFileBaseTestController {
	@Test
	public void verifyViewList(){
		/*
		 *	Test Cases 
		 *	- Open an app
		 *	- Click views
		 *	- Scroll the list till the last text   
		 * */
		appDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		appDriver.findElementByXPath("//android.widget.TextView[@text='Views']").click();
		
		appDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"WebView3\"));");
	}

}
