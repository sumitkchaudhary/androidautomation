/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 11-Dec-2021
 *  FILE NAME  		: 	 AndroidUIAutomator.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    10:49:03 pm
 */
package com.Basics.APIDemoApp;

import com.Basics.APIDemoApp.BaseClass.FetchAPIDemosAPKFileBaseTestController;
import org.testng.annotations.Test;

public class AndroidUIAutomator extends FetchAPIDemosAPKFileBaseTestController {
	
	@Test
	public void verifyViewsSize() {
		appDriver.findElementByAndroidUIAutomator("text(\"Views\")").click();
		int i=appDriver.findElementsByAndroidUIAutomator("new UiSelector().clickable(true)").size();
		System.out.println(i);
	}

}
