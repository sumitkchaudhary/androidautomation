/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 11-Dec-2021
 *  FILE NAME  		: 	 Prefences_Wifi.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    8:03:00 pm
 */
package com.Basics.APIDemoApp;

import org.testng.annotations.Test;
import org.openqa.selenium.By;

import com.Basics.APIDemoApp.BaseClass.FetchAPIDemosAPKFileBaseTestController;

public class Prefences_Wifi extends FetchAPIDemosAPKFileBaseTestController {
	@Test
	public  void verifyWifiInputFieldFunctionality(){
		
		/*		Test Cases
		 * 		- Open an app
		 * 		- Click Preference 
		 * 		- Click preference dependencies
		 * 		- Click check box on WiFI
		 * 		- Click wifi setting 
		 * 		- Provide the text into the input box 
		 * 		- Click OK button
		 * */
		appDriver.findElement(By.xpath("//android.widget.TextView[@text='Preference']")).click();
		appDriver.findElement(By.xpath("//android.widget.TextView[@text='3. Preference dependencies']")).click();
		appDriver.findElement(By.xpath("//android.widget.CheckBox[@resource-id='android:id/checkbox']")).click();
		appDriver.findElement(By.xpath("//android.widget.TextView[@text='WiFi settings']")).click();
		appDriver.findElement(By.xpath("//android.widget.EditText[@resource-id='android:id/edit']")).sendKeys("keshav@7302");
		appDriver.findElement(By.xpath("//android.widget.Button[@text='OK']")).click();
		
		
		
	}

}
