/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 12-Dec-2021
 *  FILE NAME  		: 	 DragAndDropFunctionalities.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    11:11:26 pm
 */
package com.Basics.APIDemoApp;

import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import com.Basics.APIDemoApp.BaseClass.FetchAPIDemosAPKFileBaseTestController;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.ElementOption;

public class DragAndDropFunctionalities extends FetchAPIDemosAPKFileBaseTestController {
	
	@Test
	public void verifyDragAndDropFunctionality(){
		/*
		 *	Test Cases 
		 *	- Open an app
		 *	- Click views
		 *	- Click on drag and drop option
		 *	- Then drag the first circle on the second   
		 * */
		
		appDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//appDriver.findElementByAndroidUIAutomator("attribute(\"value\")").click();
		appDriver.findElementByAndroidUIAutomator("text(\"Views\")").click();
		appDriver.findElementByAndroidUIAutomator("text(\"Drag and Drop\")").click();
		
		WebElement source=appDriver.findElementsByClassName("android.view.View").get(0);
		WebElement destination=appDriver.findElementsByClassName("android.view.View").get(1);
		
		TouchAction tap = new TouchAction(appDriver);
		
		tap.longPress(ElementOption.element(source)).moveTo(ElementOption.element(destination)).release().perform();
		
		//AssertJUnit.assertEquals("apple", "appel");
	
	}
}
