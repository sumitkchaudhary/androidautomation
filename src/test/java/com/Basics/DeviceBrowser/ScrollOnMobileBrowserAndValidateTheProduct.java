/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 25-Dec-2021
 *  FILE NAME  		: 	 ScrollOnMobileBrowserAndValidateTheProduct.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    6:59:13 pm
 */
package com.Basics.DeviceBrowser;

import com.AndroidAutomation.Utilities.MasterBaseTestController;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.MobileElement;

public class ScrollOnMobileBrowserAndValidateTheProduct extends MasterBaseTestController {
	//@Test
	public void verifyScrollFunctionalityOnMobileBrowser() throws InterruptedException{
		AndroidDriver<MobileElement> appDriver=deviceBrowserCapabilities("Android Device");
		appDriver.navigate().to("https://rahulshettyacademy.com/angularAppdemo/");
		
		WebDriverWait wait=new WebDriverWait(appDriver, 7000);
		WebElement toggerNavigation=appDriver.findElement(By.xpath("//button[@aria-label='Toggle navigation']"));
		
		wait.until(ExpectedConditions.visibilityOf(toggerNavigation)).click();
		WebElement productMenu=appDriver.findElement(By.cssSelector("a[href*='products']"));
		wait.until(ExpectedConditions.visibilityOf(productMenu)).click();
		
		JavascriptExecutor js=(JavascriptExecutor)appDriver;
		js.executeScript("window.scrollBy(0,1000)", "");
		WebElement productN=appDriver.findElement(By.xpath("(//li[@class='list-group-item']/div)[3]//div//a"));
		String productName=wait.until(ExpectedConditions.visibilityOf(productN)).getText();
		System.out.println(productName);
		//Soft assertion   : if this failed then it will allow to proceed the further validation
		SoftAssert soft=new SoftAssert();
		soft.assertEquals(productName, "Devops");
		
		//hard Assertion : If this failed then it stop the script to proceed further
		Assert.assertEquals(productName,"Devops");
		Thread.sleep(500);
		appDriver.quit();
		
	}

}
