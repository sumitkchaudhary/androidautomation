/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 14-Dec-2021
 *  FILE NAME  		: 	 ScrollItemListClickAddToCart.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    6:23:01 pm
 */
package com.Basics.ECommerceApp.TestCases;

import com.Basics.ECommerceApp.BaseClass.FetchEStoreAPKFileBaseTestController;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;

import com.Basics.ECommerceApp.CommonPages.BasicInformationLestGo;

import io.appium.java_client.MobileBy;

public class ScrollItemListClickAddToCart extends FetchEStoreAPKFileBaseTestController {
	
	@Test
	public void verifyScrollFunctionalityOnProduct() {
		/* Test Scenario	: Click the item from the list and add to cart then click the purchase
		 * Test Case		: Login > Scroll the list and click add to cart from the desire item > Click to purchase
		 * 
		 */
		
		//Wait for device connectivity
		appDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//Call the form filling controller and provide the name of the user
		BasicInformationLestGo firstStep=new BasicInformationLestGo(appDriver);
		firstStep.handleLetGoProcess("Sumit Kumar");
		//Scroll the list as per the item name
		appDriver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.androidsample.generalstore:id/rvProductList\")).scrollIntoView(new UiSelector().textMatches(\"Jordan 6 Rings\").instance(0))"));
	    //Get the size of the item from list
		int count=    appDriver.findElements(By.id("com.androidsample.generalstore:id/productName")).size();
	    for(int i=0;i<count;i++)
	    {
	    	//get the text of the item from list 
	    	String text=appDriver.findElements(By.id("com.androidsample.generalstore:id/productName")).get(i).getText();
	    	//Compare the item name with desire item 
	    	if(text.equalsIgnoreCase("Jordan 6 Rings"))
	    	{
	    		//Click that item which match with desired 
	    		appDriver.findElements(By.id("com.androidsample.generalstore:id/productAddCart")).get(i).click();
	    		//Exit from the loop
	    		break;
	    	}
	    }
	    //Click the add to cart from the matched item 
	    appDriver.findElement(By.id("com.androidsample.generalstore:id/appbar_btn_cart")).click();
	    //Click checkbox for visit to purchase 
	    appDriver.findElementByClassName("android.widget.CheckBox").click();
	    appDriver.findElementById("com.androidsample.generalstore:id/btnProceed").click();
	}

}
