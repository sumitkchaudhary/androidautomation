/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 14-Dec-2021
 *  FILE NAME  		: 	 FormFilling_VerifyValidationMessage.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    9:23:22 am
 */
package com.Basics.ECommerceApp.TestCases;

import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import com.Basics.ECommerceApp.BaseClass.FetchEStoreAPKFileBaseTestController;
import com.Basics.ECommerceApp.CommonPages.BasicInformationLestGo;

public class FormFilling_VerifyValidationMessage extends FetchEStoreAPKFileBaseTestController {
	
	@Test
	public void verifyValidationMessageForName() {
		/* Test Scenario 	:	Verify the validation pop up message when user trying to proceed further process without enter his name. 	
		 * Test Case 
		 * 	- Open E-Commerce App 
		 * 	- Click the country drop down list
		 * 	- Select 'India' from the list
		 * 	- Left Name input box blank 
		 * 	- Select Gender (For now select Female)
		 * 	- Click the Let's go button 
		 * 	- Get the pop up message
		 *  - verify accordingly
		 * */
		
		appDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		BasicInformationLestGo firstProcess=new BasicInformationLestGo(appDriver);
		
		firstProcess.handleLetGoProcess("");
		String toastMessage=appDriver.findElement(By.xpath("//android.widget.Toast[1]")).getAttribute("name");
		System.out.println(toastMessage);
		if(toastMessage.equals("Please enter your name")){
			System.out.println("Test Case Pass");
		}else {
			System.out.println("Test Case Failed");
		}
	}

}
