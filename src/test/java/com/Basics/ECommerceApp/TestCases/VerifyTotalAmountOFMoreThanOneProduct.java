/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 23-Dec-2021
 *  FILE NAME  		: 	 VerifyTotalAmountOFMoreThanOneProduct.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    7:41:16 am
 */
package com.Basics.ECommerceApp.TestCases;

import org.testng.annotations.Test;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Basics.ECommerceApp.BaseClass.FetchEStoreAPKFileBaseTestController;
import com.Basics.ECommerceApp.CommonPages.BasicInformationLestGo;

import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;

public class VerifyTotalAmountOFMoreThanOneProduct extends FetchEStoreAPKFileBaseTestController {
	@Test
	public void verifyCompareTotalAmount() throws InterruptedException {
		appDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		BasicInformationLestGo firstStep=new BasicInformationLestGo(appDriver);
		firstStep.handleLetGoProcess("Sumit Kumar");
		
		appDriver.findElements(By.xpath("//*[@text='ADD TO CART']")).get(0).click();
		appDriver.findElements(By.xpath("//*[@text='ADD TO CART']")).get(0).click();
		appDriver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();
		Thread.sleep(4000);
		int productSize=appDriver.findElementsById("com.androidsample.generalstore:id/productPrice").size();
		
		double expectedTotalPriceOfProducts=0;
		
		for(int i=0; i<productSize;i++) {
			String productValue=appDriver.findElementsById("com.androidsample.generalstore:id/productPrice").get(i).getText();
			double price=getAmountWithoutDoller(productValue);
			System.out.println("Product "+i+"=" +price);
			expectedTotalPriceOfProducts=expectedTotalPriceOfProducts+price;
			System.out.println("My Calculated value of products  "+expectedTotalPriceOfProducts);
		}
		
		String actualTotalPrice=appDriver.findElementById("com.androidsample.generalstore:id/totalAmountLbl").getText();
		System.out.println("Actual Total is  "+getAmountWithoutDoller(actualTotalPrice));
		if(expectedTotalPriceOfProducts==getAmountWithoutDoller(actualTotalPrice)) {
			System.out.println("Test Case Pass");
			//Mobile Gestures

			WebElement checkbox=appDriver.findElement(By.className("android.widget.CheckBox"));

			TouchAction t=new TouchAction(appDriver);
			t.tap(TapOptions.tapOptions().withElement(ElementOption.element(checkbox))).perform();
			WebElement tc=appDriver.findElement(By.xpath("//*[@text='Please read our terms of conditions']"));

			t.longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(tc)).withDuration(Duration.ofSeconds(2))).release().perform();

			appDriver.findElement(By.id("android:id/button1")).click();

			appDriver.findElement(By.id("com.androidsample.generalstore:id/btnProceed")).click();

		}else {
			System.out.println("Test Case Fail");
		}
	}
	
	public static double getAmountWithoutDoller(String productPrice) {
		
		productPrice=productPrice.substring(1);
		double price=Double.parseDouble(productPrice);
		return price;
	}

}
