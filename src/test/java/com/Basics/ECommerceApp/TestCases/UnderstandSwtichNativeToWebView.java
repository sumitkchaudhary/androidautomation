/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 23-Dec-2021
 *  FILE NAME  		: 	 UnderstandSwtichNativeToWebView.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    9:24:26 am
 */
package com.Basics.ECommerceApp.TestCases;

import org.testng.annotations.Test;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.Basics.ECommerceApp.BaseClass.FetchEStoreAPKFileBaseTestController;
import com.Basics.ECommerceApp.CommonPages.BasicInformationLestGo;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class UnderstandSwtichNativeToWebView extends FetchEStoreAPKFileBaseTestController {
	
	@Test
	public void verifyNativeToWebView() throws InterruptedException{
		/*	Test Case	:	verify if user can do operations on web view and can navigate back to navigate app if needed
		 * 	Test Steps	:	Open an app 
		 * 					Fill the form which opened initially
		 * 					Select anyone product from the list
		 * 					Click the cart after selected item added into it
		 * 					Click the continue to purchase button 
		 * 					and check its native or webview
		 * 					after that switch to native app to webview
		 * 					perform some action on that browser
		 * 					Return back on the native app by click device back button
		 * */
		//Click the basic configuration which is handling the connection of the device as per parameter
		//Wait 10 second for launch the app
		appDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//call the method which is handling to fill the form and pass the parameter
		BasicInformationLestGo firstStep=new BasicInformationLestGo(appDriver);
		firstStep.handleLetGoProcess("Sumit Kumar");
		
		//Click add to cart from the product 
		appDriver.findElements(By.xpath("//*[@text='ADD TO CART']")).get(0).click();
		//click cart button 
		appDriver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();
		Thread.sleep(4000);
		//click the checkbox for proceed to purchase the selected item
		WebElement checkbox=appDriver.findElement(By.className("android.widget.CheckBox"));
		TouchAction t=new TouchAction(appDriver);
		appDriver.findElement(By.id("com.androidsample.generalstore:id/btnProceed")).click();
		//wait for 7 second
		Thread.sleep(7000);
		//here verify this app is native or hybrid 
		Set<String> contexts=appDriver.getContextHandles();
		
		for(String contextName: contexts) {
			System.out.println(contextName);
			/*	Expected	
			 * 	NATIVE_APP
			 * 	WEBVIEW_com.androidsample.generalstore
			 * */
		}
		//wait until the browser launch
		Thread.sleep(7000);
		//pass the parameter to switch the browser 
		appDriver.context("WEBVIEW_com.androidsample.generalstore");
		WebDriverWait wait=new WebDriverWait(appDriver, 7000);
		//Enter the hello in the google search bar and hit the enter 
		WebElement searchBar=appDriver.findElement(By.xpath("//input[@name='q']"));
		wait.until(ExpectedConditions.visibilityOf(searchBar)).sendKeys("hello");
		searchBar.sendKeys(Keys.ENTER);
		//click the device back button 
		appDriver.pressKey(new KeyEvent(AndroidKey.BACK));
	}
	
}
