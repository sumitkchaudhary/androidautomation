/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 30-Dec-2021
 *  FILE NAME  		: 	 FetchAPIDemosAPKFileBaseTestController.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    10:44:21 pm
 */
package com.Basics.ECommerceApp.BaseClass;

import com.AndroidAutomation.Utilities.MasterBaseTestController;
import org.testng.annotations.BeforeMethod;

public class FetchEStoreAPKFileBaseTestController extends MasterBaseTestController {
	@BeforeMethod
	public void getAPKFile() {
		appDriver=deviceCapabilities("Emulator", "dummyEStore");
	}

}
