/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 14-Dec-2021
 *  FILE NAME  		: 	 BasicInformationLestGo.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    6:12:47 pm
 */
package com.Basics.ECommerceApp.CommonPages;

import org.openqa.selenium.By;

import com.AndroidAutomation.Utilities.HandleScrolling;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class BasicInformationLestGo {
	AndroidDriver<MobileElement> appDriver;
	public BasicInformationLestGo(AndroidDriver<MobileElement> appDriver) {
		this.appDriver=appDriver;
	}
	
	public void handleLetGoProcess(String userName) {
		appDriver.findElement(By.xpath("//android.widget.Spinner[@resource-id='com.androidsample.generalstore:id/spinnerCountry']")).click();
		HandleScrolling scrollList=new HandleScrolling(appDriver);
		scrollList.listScroll("India");
		appDriver.findElement(By.xpath("//android.widget.TextView[@text='India']")).click();
		
		appDriver.findElement(By.xpath("//android.widget.EditText[@text='Enter name here']")).sendKeys(userName);
		appDriver.hideKeyboard();
		appDriver.findElement(By.xpath("//android.widget.RadioButton[@text='Male']")).click();//Male
		appDriver.findElement(By.id("com.androidsample.generalstore:id/btnLetsShop")).click();
	
	}
}
