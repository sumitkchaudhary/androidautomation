/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 01-Jan-2022
 *  FILE NAME  		: 	 TestCaseListener.java
 *  PROJECT NAME 	:	 AndroidAppAutomation
 * 	Class Time		:    11:18:21 pm
 */
package com.AndroidAutomation.ReportListeners;

import java.io.IOException;

import com.AndroidAutomation.Utilities.MasterBaseTestController;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestCaseListener implements ITestListener{
	

	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	public void onTestFailure(ITestResult result) {
		
		//screenshot 
		try {
		MasterBaseTestController.getScreenShot(MasterBaseTestController.fetchCurrentDateTime()+result.getName().getClass().getSimpleName());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}
	
}
